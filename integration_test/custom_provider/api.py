import frappe
from frappe.utils.response import build_response


@frappe.whitelist(allow_guest=True)
def get_token_email():

    frappe.local.response.http_status_code = 200
    frappe.local.response.update(
        {
            "data": {
                "emailId": "anonymous.user@example.com",
                "fullName": "Anonymous",
            }
        }
    )

    return build_response("json")


@frappe.whitelist(allow_guest=True)
def get_token_no_email():
    frappe.local.response.http_status_code = 200
    frappe.local.response.update(
        {
            "data": {
                "domain": "anonymous.user",
                "fullName": "Anonymous",
                "employee_id": "EMP-000",
                "mobile": "9876543210",
                "host": "example.com",
            },
        }
    )

    return build_response("json")


@frappe.whitelist(allow_guest=True)
def get_token_error():
    frappe.local.response.http_status_code = 200
    frappe.local.response.update(
        {
            "status": "failed to decrypt",
            "message": "",
        }
    )

    return build_response("json")
